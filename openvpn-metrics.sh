#!/bin/sh

# Execute a metrics-generating script and safely write its
# output to /tmp/prometheus
#
#
output_dir="/tmp/prometheus"
output_file="${output_dir}/openvpn.prom"
umask 022
tmp_file="${output_file}.$$"
trap "rm -f $tmp_file 2>/dev/null" EXIT INT TERM

mkdir -p $output_dir

echo "# HELP openvpn_up Whether scraping OpenVPN's metrics was successful." > $tmp_file
echo '# TYPE openvpn_up gauge' >> $tmp_file
echo -n 'openvpn_up{status_path="/tmp/openvpn-status-tcp"} ' >> $tmp_file
if [ -r /tmp/openvpn-status-tcp ];
then
    echo "1" >> $tmp_file
    echo "# HELP openvpn_server_connected_clients Number Of Connected Clients" >> $tmp_file
    echo "# TYPE openvpn_server_connected_clients gauge" >> $tmp_file
    tcp_connected=$(cat /tmp/openvpn-status-tcp |grep CLIENT_LIST|wc -l)
    echo -n 'openvpn_server_connected_clients{status_path="/tmp/openvpn-status-tcp"} ' >> $tmp_file
    echo $tcp_connected >> $tmp_file
else
    echo "0" >> $tmp_file
fi

echo -n 'openvpn_up{status_path="/tmp/openvpn-status-udp"} ' >> $tmp_file
if [ -r /tmp/openvpn-status-udp ];
then
    echo "1" >> $tmp_file
    udp_connected=$(cat /tmp/openvpn-status-udp |grep CLIENT_LIST|wc -l)
    echo -n 'openvpn_server_connected_clients{status_path="/tmp/openvpn-status-udp"} ' >> $tmp_file
    echo $udp_connected >> $tmp_file
else
    echo "0" >> $tmp_file
fi

mv -f "$tmp_file" "$output_file"

exit $?
