FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:v12.0.0

COPY kresd.key /usr/share/keyrings/kresd.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/kresd.gpg] https://download.opensuse.org/repositories/home:/CZ-NIC:/knot-resolver-latest/Debian_12/ /" > /etc/apt/sources.list.d/kresd.list

RUN apt-get -q update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends \
  libcap2-bin \
  netcat-openbsd \
  iptables \
  iproute2 \
  knot-resolver \
  knot-resolver-module-http \
  openvpn \
  socat \
  lua-socket \
  lua-sec \
  prometheus-node-exporter \
  && rm -rf /var/lib/apt/lists/*

# Remove unwanted prometheus-node-exporter scripts installed by the package
RUN rm -f /var/lib/prometheus-node-exporter/*

RUN setcap cap_net_admin,cap_net_bind_service+ep /usr/sbin/openvpn
RUN setcap cap_net_admin+ep /bin/ip
RUN setcap cap_net_bind_service+ep /usr/sbin/kresd
RUN update-alternatives --set iptables /usr/sbin/iptables-legacy

# S6 configuration
COPY conf/ /etc/
COPY openvpn-metrics.sh /usr/local/sbin/openvpn-metrics.sh
